a__author__ = 'jacob'
from bidict import BidirectionalMapping

"""
Contains one function: 'assign_id(words)'
"""


def assign_id(words):
	"""
	Assigns ID's in numeric order starting at 0 to unique values.
	:param words: contains the elements that will be given IDs
	:return: list_id: a dictionary of that has the IDs (0 - list.length -1) as the keys and elements of lists as values
	"""
	count = 0
	list_id = {}
	for name in words:

		# Make all strings lowercase
		if isinstance(name, str):
			name = name.lower()

		# Add name if it is not in the list already and set its ID to count
		if name not in list_id.keys():
			list_id.update({name: count})
			count += 1

	# Make list of authors' names and IDs into a bidirectional mapping, which can be thought
	# of as a two-way dictionary (in other words, a key can be looked up by a value and
	# vice-versa)
	bi_dict = BidirectionalMapping(list_id)

	return bi_dict
