"""
Does the time slice for the output. Requires a command line
argument of an integer to use for the time slice.
For example: 20

Gets the information from date_docs.txt and then prints output
to time_slice.txt and time_slice_IDs.txt.

time_slice.txt has the first day of the time slice followed by
a tab and the number of documents created in that time slice.

time_slice_IDs.txt has the first day of the time slice followed
by a tab and a list of the document IDs for the documents
created in that time slice.
"""

from __future__ import print_function
import sys
from datetime import datetime
from datetime import timedelta

time_slice = int(sys.argv[1])  # The number of days for the time slice, given by user
dates = []  # List of all the dates
date_doc_dict = {}  # Key = first day of time slice, value = list of doc IDS (as a string)
date_count_dict = {}  # Key = date, value = number of documents created for that day or time slice

# Just a bit of error checking to prevent modifying files in an unpleasant way
if time_slice < 1 or not isinstance(time_slice, int):
    sys.exit('Invalid time slice. Ending program.')

print('Reading date_docs.txt...')

# Get the data from the input file and put it into lists and dictionaries
with open('date_docs.txt', 'r') as f:
    for line in f:
        line_str = line.strip()
        date_str, doc_ID = map(str.strip, line_str.split('\t'))
        date = datetime.strptime(date_str, '%Y-%m-%d')  # Turn the date from a string to a datetime object
        dates.append(date)  # Add the date to the list of dates

        # Add date into date_doc_dict if it is not already there and make the corresponding
        # document ID its value. If the date is already in this dictionary, modify the
        # value of the date key in the dictionary to reflect the dates
        if date not in date_doc_dict:
            date_doc_dict[date] = str(doc_ID)
        else:
            date_doc_dict[date] += ' ' + str(doc_ID)

dates.sort()  # Sorting the dates so they are in linear order
day_one = dates[0]  # the very first date appearing out of all the documents

print('Calculating documents per day...')

# Store the dates with their keys as the number of documents created on that day
for date in dates:
    # Add the date to the dictionary if it not already in there
    if date not in date_count_dict:
        date_count_dict[date] = 1
    else:
        # If date is already in dictionary, increment its count
        date_count_dict[date] += 1

print('Calculating documents per time slice...')

time_slice_log = open('time_slice_IDs.txt', 'w')  # Will print the time slice date then the doc IDs

# This loop will handle the different time slices and print the output to time_slice.txt and time_slice_IDs.txt
with open('time_slice.txt', 'w') as f:  # first day of the time slice date followed by the number of documents created
    current_day = day_one  # The current day we are working with
    total_days_int = (max(dates) - day_one).days  # integer representing the total number of days to go through
    date_format = '%Y-%m-%d'  # the format to print the dates in

    # The code is much simpler if the time slice is 1, which is why it gets its own loop
    if int(time_slice) == 1:
        day_change = timedelta(days=1)  # Changes the current day by the time slice

        # This loop goes through every single day from day_one to the very last day and prints output to both
        # time_slice.txt and time_slice_IDs.txt
        for i in range(0, total_days_int + 1):
            if current_day in date_count_dict:
                f.write(str(current_day.strftime(date_format)) + '\t' + str(date_count_dict[current_day]) + '\n')
                print(str(current_day.strftime(date_format)) + '\t' + date_doc_dict[current_day], file=time_slice_log)
            else:
                f.write(str(current_day.strftime(date_format) + '\t0\n'))
                print(str(current_day.strftime(date_format)), file=time_slice_log)

            current_day += day_change  # Increments to the next day

    # This next section accounts for if the time slice is greater than 1.
    else:
        max_day = int(time_slice) - 1  # The last day in the current slice of time
        i = 0  # an index to count all of the days
        document_id_string = ''  # a string to keep track of all the doc IDs to print to time_slice_IDs.txt
        day_change = timedelta(days=time_slice)  # the number of days to change the time slice by

        # This loop goes through all of the days and sorts everything into the given time slice. Prints the output
        # to both time_slice.txt and time_slice_IDs.txt
        while i <= total_days_int:
            total_docs = 0  # the number of documents created in that time slice
            secondary_day_change = timedelta(days=1)  # Need to go through each individual day within the time slice
            secondary_current_day = current_day  # All the days within a time slice (can not affect the first day
            # of a time slice)

            # Goes through each day in the current chunk of time and tallies all the variables for the output
            # to time_slice.txt and time_slice_IDs.txt
            for index in range(i, max_day + 1):
                if secondary_current_day in date_count_dict:
                    total_docs += date_count_dict[secondary_current_day]
                    document_id_string += str(date_doc_dict[secondary_current_day]) + ' '

                secondary_current_day += secondary_day_change  # Go to the next day

            # Write to the files
            f.write(str(current_day.strftime(date_format)) + '\t' + str(total_docs) + '\n')
            print(str(current_day.strftime(date_format)) + '\t' + document_id_string, file=time_slice_log)

            max_day += int(time_slice)  # Increment by the time slice
            i += int(time_slice)  # Increment by the time slice
            current_day += day_change  # Increment to the first day of the next time slice
            document_id_string = ''  # Reset the string with the list of document IDs

# Ending message
print('Program Completed')
