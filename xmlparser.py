import xmltodict
from datetime import datetime

from document import Document


def parse_xml_docs(fd):
    """
    Runs through the given xml document and stores the authors and contributors,
    dates, abstract (if there is one), and the body text.
    :param fd: the xml document to read
    :return: A list object containing the list of contributors, the abstract text and body text,
             and a Document object with the contributors, abstract and body texts, and original
             publication date.
    """
    doc = xmltodict.parse(fd.read())
    article_meta = doc['article']['front']['article-meta']

    # Store all contributing authors and collaborations in a list.
    contributors = []
    if 'contrib-group' in article_meta:
        # NOTE: last names are normally stored under the tag 'surname' and first names are normally stored under
        # the tag 'given-names'. We also use '.encode('ascii', 'ignore')' to ignore otherwise problematic characters

        # Need to check for the tag 'contrib' in 'contrib-group'
        if 'contrib' in article_meta['contrib-group']:

            # There is sometimes an extra class the names are under called 'name-alternatives'
            if 'name-alternatives' in article_meta['contrib-group']['contrib']:

                # Continue if 'name-alternatives' is a dictionary object
                if (article_meta['contrib-group']['contrib']['name-alternatives'].__class__.__name__ == 'OrderedDict') \
                        or isinstance(article_meta['contrib-group']['contrib']['name-alternatives'], dict):
                    for name in article_meta['contrib-group']['contrib']['name-alternatives']:
                        if name == u'name':
                            for item in article_meta['contrib-group']['contrib']['name-alternatives']['name']:
                                if (item.__class__.__name__ == 'OrderedDict') and (len(item) == 2):
                                    # If there is a name, append it to the list of contributors
                                    if ('given-names' in item) and ('surname' in item):
                                        contributors.append(str(
                                            item['given-names'].encode('ascii', 'ignore') + ' ' + item[
                                                'surname'].encode('ascii', 'ignore')))

            # This is normally how the names are categorized
            else:
                for name in article_meta['contrib-group']['contrib']:
                    if (name.__class__.__name__ == 'OrderedDict') or isinstance(name, dict):
                        if 'name' in name:

                            # Need to make sure both first and last names are actually there and are not 'NoneType'
                            # if ('given-names' in name['name']) and ('surname' in name['name']) and not (name['name']['given-names'].__class__.__name__ == 'NoneType') and not (name['name']['surname'].__class__.__name__ == 'NoneType'):
                            if 'given-names' in name['name'] and 'surname' in name['name']:
                                if name['name']['given-names'] and name['name']['surname']:
                                    # If 'given-names' is an OrderedDict need to get its contents
                                    if name['name']['given-names'].__class__.__name__ == 'OrderedDict':
                                        if '#text' in name['name']['given-names']:
                                            # if 'surname' in name['name']:
                                            if name['name']['surname']:
                                                if name['name']['given-names']['#text'].__class__.__name__ == 'unicode' \
                                                        or name['name']['surname'].__class__.__name__ == 'unicode':
                                                    contributors.append(str(
                                                        name['name']['given-names']['#text'].encode('ascii',
                                                                                                    'ignore') + ' ' +
                                                        name['name']['surname'].encode('ascii', 'ignore')))
                                            # elif not name['name']['given-names']['#text'].__class__.__name__ == 'NoneType':
                                            elif name['name']['given-names']['#text']:
                                                contributors.append(
                                                    str(name['name']['given-names'].encode('ascii', 'ignore'
                                                                                           ) + ' *'))

                                    # Otherwise if 'surname' is an OrderedDict need to get its contents
                                    elif name['name']['surname'].__class__.__name__ == 'OrderedDict':
                                        if '#text' in name['name']['surname']:
                                            # if ('given-names' in name['name']) and not (name['name']['given-names'].__class__.__name__ == 'NoneType'):
                                            if 'given-names' in name['name']:
                                                if name['name']['given-names']:
                                                    if name['name']['given-names'].__class__.__name__ == 'unicode' or \
                                                                    name['name']['surname'][
                                                                        '#text'].__class__.__name__ == \
                                                                    'unicode':
                                                        contributors.append(str(name['name']['given-names'].encode(
                                                            'ascii', 'ignore') + ' ' + name['name']['surname']['#text'].
                                                                                encode('ascii', 'ignore')))
                                                # elif not name['name']['surname']['#text'].__class__.__name__ == 'NoneType':
                                                elif name['name']['surname']['#text']:
                                                    contributors.append(str('* ' + name['name']['surname']['#text'].
                                                                            encode('ascii', 'ignores')))

                                    # If both are unicode, need to format
                                    elif name['name']['given-names'].__class__.__name__ == 'unicode' or name['name'][
                                        'surname'].__class__.__name__ == 'unicode':
                                        contributors.append(str(name['name']['given-names'].encode('ascii', 'ignore') +
                                                                ' ' + name['name']['surname'].encode('ascii',
                                                                                                     'ignore')))
                                    else:
                                        contributors.append(
                                            str(name['name']['given-names'] + ' ' + name['name']['surname']))

                            # If there is not both a first and a last name, check that there is a first name
                            # elif ('given-names' in name['name']) and not (name['name']['given-names'].__class__.__name__ == 'NoneType'):
                            elif 'given-names' in name['name'] and name['name']['given-names']:
                                if name['name']['given-names'].__class__.__name__ == 'unicode':
                                    contributors.append(str(name['name']['given-names'].encode('ascii', 'ignore') + ' *'
                                                            ))
                                else:
                                    contributors.append(str(name['name']['given-names'] + ' *'))

                            # If no first name, check for a last name
                            # elif ('surname' in name['name']) and not (name['name']['surname'].__class__.__name__ == 'NoneType'):
                            elif 'surname' in name['name'] and name['name']['surname']:
                                if name['name']['surname'].__class__.__name__ == 'unicode':
                                    contributors.append(str('* ' + name['name']['surname'].encode('ascii', 'ignore')))
                                else:
                                    contributors.append(str('* ' + name['name']['surname']))

                    # Articles with only one contributor are not OrderedDict, so must account for that
                    else:
                        if 'name' in name:
                            # If both last name and first name are present and not NoneType, can go ahead and use them
                            """
                            if ('given-names' in article_meta['contrib-group']['contrib']['name']) \
                                    and ('surname' in article_meta['contrib-group']['contrib']['name'])\
                                    and not (article_meta['contrib-group']['contrib']['name']['given-names'].__class__.__name__ == 'NoneType')\
                                    and not (article_meta['contrib-group']['contrib']['name']['surname'].__class__.__name__ == 'NoneType'):
                            """
                            """
                            if article_meta['contrib-group']['contrib']['name']['given-names'] and article_meta['contrib-group']['contrib']['name']['surname']:
                                if article_meta['contrib-group']['contrib']['name']['given-names'].__class__.__name__ == 'unicode' or article_meta['contrib-group']['contrib']['name']['surname'].__class__.__name__ == 'unicode':
                                    contributors.append(str(article_meta['contrib-group']['contrib']['name']['given-names'].encode('ascii', 'ignore')
                                    + ' ' + article_meta['contrib-group']['contrib']['name']['surname'].encode('ascii', 'ignore')))
                                else:
                                    contributors.append(str(article_meta['contrib-group']['contrib']['name']['given-names'])) + ' ' + article_meta['contrib-group']['contrib']['name']['surname'].encode('ascii', 'ignore')
                            """
                            name_long = doc['article']['front']['article-meta']['contrib-group']['contrib']['name']
                            if 'given-names' in name_long and 'surname' in name_long:
                                if name_long['given-names'] and name_long['surname']:
                                    if name_long['given-names'].__class__.__name__ == 'unicode' or name_long['surname'] \
                                            .__class__.__name__ == 'unicode':
                                        contributors.append(str(name_long['given-names'].encode('ascii', 'ignore') +
                                                                ' ' + name_long['surname'].encode('ascii', 'ignore')))
                                    else:
                                        contributors.append(str(name_long['given-names'].encode('ascii', 'ignore') +
                                                                ' ' + name_long['surname'].encode('ascii', 'ignore')))

                            # Otherwise, if the first name is present and not NoneType, record
                            # elif 'given-names' in 'name' and not (article_meta['contrib-group']['contrib']['name']['given-names'].__class__.__name__ == 'NoneType'):
                            # elif article_meta['contrib-group']['contrib']['name']['given-names']:
                            elif 'given-names' in name_long and name_long['given-names']:
                                """
                                if article_meta['contrib-group']['contrib']['name']['given-names'].__class__.__name__ == 'unicode':
                                    contributors.append(str(article_meta['contrib-group']['contrib']['name']['given-names'].encode('ascii', 'ignore') + ' *'))
                                else:
                                    contributors.append(str(article_meta['contrib-group']['contrib']['name']['given-names'] + ' *'))
                                """
                                if name_long['given-names'].__class__.__name__ == 'unicode':
                                    contributors.append(str(name_long['given-names'].encode('ascii', 'ignore') + ' *'))
                                else:
                                    contributors.append(str(name_long['given-names'] + ' *'))

                            # If the last name is present and not NoneType, record
                            # elif 'surname' in 'name' and not (article_meta['contrib-group']['contrib']['name']['surname'].__class__.__name__ == 'NoneType'):
                            # elif article_meta['contrib-group']['contrib']['name']['surname']:
                            elif 'surname' in name_long and name_long['surname']:
                                """
                                if article_meta['contrib-group']['contrib']['name']['surname'].__class__.__name__ == 'unicode':
                                    contributors.append(str('* ' + article_meta['contrib-group']['contrib']['name']['surname'].encode('ascii', 'ignore')))
                                else:
                                    contributors.append(str('* ' + article_meta['contrib-group']['contrib']['name']['surname']))
                                """
                                if name_long['surname'].__class__.__name__ == 'unicode':
                                    contributors.append(str('* ' + name_long['surname'].encode('ascii', 'ignore')))
                                else:
                                    contributors.append(str('* ' + name_long['surname']))


        # If 'contrib' is not in 'contrib-group'
        else:
            for name in article_meta['contrib-group']:

                # Proceed if each name object is an OrderedDict or dictionary
                if (name.__class__.__name__ == 'OrderedDict') or isinstance(name, dict):

                    # For every entry in name
                    for entry in name:
                        if entry == 'contrib':

                            # If 'contrib' contains the tag 'name'
                            if 'name' in name['contrib']:
                                # if ('surname' in name['contrib']['name']) and ('given-names' in name['contrib']['name']):
                                if name['contrib']['name']['surname'] and name['contrib']['name']['given-names']:
                                    # if not (name['contrib']['name']['surname'].__class__.__name__ == 'NoneType') and not (name['contrib']['name']['given-names'].__class__.__name__ == 'NoneType'):
                                    contributors.append(str(
                                        name['contrib']['name']['given-names'].encode('ascii', 'ignore') + ' ' +
                                        name['contrib']['name']['surname'].encode('ascii', 'ignore')))
                                # elif 'given-names' in name['contrib']['name']:
                                elif name['contrib']['name']['given-names']:
                                    # if not name['contrib']['name']['given-names'].__class__.__name__ == 'NoneType':
                                    contributors.append(
                                        (str(name['contrib']['name']['given-names'].encode('ascii', 'ignore') + ' *')))
                                # elif 'surname' in name['contrib']['name']:
                                elif name['contrib']['name']['surname']:
                                    # if not name['contrib']['name']['surname'].__class__.__name__ == 'NoneType':
                                    contributors.append(
                                        str('* ' + name['contrib']['name']['surname'].encode('ascii', 'ignore')))

                            # If 'contrib' does not contain the tag 'name' it should have a list of OrderedDicts
                            for item in name['contrib']:
                                if item.__class__.__name__ == 'OrderedDict':
                                    if 'name' in item:
                                        # if ('surname' in item['name']) and ('given-names' in item['name']):
                                        if item['name']['surname'] and item['name']['given-names']:
                                            # if not (item['name']['surname'].__class__.__name__ == 'NoneType') and not (item['name']['given-names'].__class__.__name__ == 'NoneType'):
                                            contributors.append(str(
                                                item['name']['given-names'].encode('ascii', 'ignore') + ' ' +
                                                item['name']['surname'].encode('ascii', 'ignore')))
                                        # elif 'given-names' in item['name']:
                                        elif item['name']['given-names']:
                                            # if not item['name']['given-names'].__class__.__name__ == 'NoneType':
                                            contributors.append(
                                                (str(item['name']['given-names'].encode('ascii', 'ignore') + ' *')))
                                        # elif 'surname' in name['contrib']['name']:
                                        elif name['contrib']['name']['surname']:
                                            # if not item['name']['surname'].__class__.__name__ == 'NoneType':
                                            contributors.append(
                                                str('* ' + item['name']['surname'].encode('ascii', 'ignore')))

    # Skip the document if we have no contributors
    if not contributors:
        return None

    # Get rid of duplicate authors (authors are sometimes listed twice in a document)
    contributors = list(set(contributors))

    # Store off the earliest publication date as the original date in a list.
    dates = []
    if 'pub-date' in article_meta:

        # Occasionally, the 'pub-date' tag contains a list of ordered dictionaries, so we must go through
        # and parse for each of these. If the month or day value is missing, we use 1 instead
        for date in article_meta['pub-date']:
            date_str = ''
            if date.__class__.__name__ == 'OrderedDict':
                if 'year' in date and not date['year'].__class__.__name__ == 'OrderedDict':
                    date_str += date['year'] + '/'
                # elif ('year' in date) and ('#text' in date['year']):
                elif date['year']['#text']:
                    date_str += date['year']['#text'].encode('ascii', 'ignore') + '/'

                if 'month' in date:
                    date_str += date['month'] + '/'
                else:
                    date_str += '1' + '/'

                if 'day' in date:
                    date_str += date['day']
                else:
                    date_str += '1'

                # Make all the dates a datetime object parsed according to the given format
                dates.append(datetime.strptime(date_str, '%Y/%m/%d'))

        # the 'pub-date' tag is sometimes an ordered dictionary with just one date, so it is easy to get the date
        if article_meta['pub-date'].__class__.__name__ == 'OrderedDict':
            date_str = ''
            if 'year' in article_meta['pub-date']:
                date_str += article_meta['pub-date']['year'] + '/'

            if 'month' in article_meta['pub-date']:
                date_str += article_meta['pub-date']['month'] + '/'
            else:
                date_str += '1' + '/'

            if 'day' in article_meta['pub-date']:
                date_str += article_meta['pub-date']['day']
            else:
                date_str += '1'

            # Make all the dates a datetime object parsed to the according format
            dates.append(datetime.strptime(date_str, '%Y/%m/%d'))

    # The original publication date for the document is the first calendar date in the document
    original_pub_date = min(dates)

    # Store off the abstract
    abstract = ''
    if 'abstract' in article_meta:
        if 'p' in article_meta['abstract']:
            abstract_section = article_meta['abstract']['p']
            # if not (abstract_section.__class__.__name__ == 'NoneType') and ('#text' in abstract_section):
            if abstract_section and '#text' in abstract_section:

                # Need to check that abstract is unicode and change it into a string if it is - MH
                if abstract_section['#text'].__class__.__name__ == 'unicode':
                    abstract = str(abstract_section['#text'].encode('ascii', 'ignore'))
                else:
                    abstract = str(abstract_section['#text'])
            else:
                if abstract_section.__class__.__name__ == 'unicode':
                    abstract = str(abstract_section.encode('ascii', 'ignore'))
                else:
                    abstract = str(abstract_section)

    # Store off the body
    body = ''
    if 'body' in doc['article']:
        article_body = doc['article']['body']
        for blank in article_body:

            # If we are dealing with just a paragraph...
            if blank == u'p':
                for key in article_body['p']:
                    # if not key.__class__.__name__ == 'NoneType':
                    if key:
                        for key1 in key:
                            if key1 == ['#text']:
                                if key['#text'].__class__.__name__ == 'unicode':
                                    body += str((key['#text']).encode('ascii', 'ignore'))
                                elif key['#text'].__class__.__name__ == 'str':
                                    body += key['#text']

                    if key == u'#text':
                        if article_body['p']['#text'].__class__.__name__ == 'unicode':
                            body += str((article_body['p']['#text']).encode('ascii', 'ignore'))
                        else:
                            body += str(article_body['p']['#text'])

            # Most articles will have sections surrounding either recursive sections or just paragraphs. For this
            # section, there are many embedded lists and OrderedDicts, making for a complicated structure
            if blank == u'sec':
                for key in article_body['sec']:
                    for key1 in key:
                        if key1 == u'sec':
                            if key.__class__.__name__ == 'OrderedDict':
                                for key2 in key[key1]:
                                    if key2.__class__.__name__ == 'OrderedDict':
                                        for key3 in key2:
                                            if key3 == u'p':
                                                if key2['p'].__class__.__name__ == 'OrderedDict':
                                                    for temp in key2['p']:
                                                        if temp == u'#text':
                                                            if key2['p']['#text'].__class__.__name__ == 'unicode':
                                                                body += str(key2['p']['#text'].encode('ascii', 'ignore')
                                                                            )
                                                            else:
                                                                body += str(key2['p']['#text'])
                                                # elif not key2['p'].__class__.__name__ == 'NoneType':
                                                elif key2['p']:
                                                    for temp in key2['p']:
                                                        if temp == u'#text':
                                                            if key2['p']['#text'].__class__.__name__ == 'unicode':
                                                                body += str(key2['p']['#text'].encode('ascii', 'ignore')
                                                                            )
                                                            else:
                                                                body += str(key2['p']['#text'])
                                                        elif temp.__class__.__name__ == 'OrderedDict':
                                                            for temp1 in temp:
                                                                if temp1 == u'#text':
                                                                    if temp['#text'].__class__.__name__ == 'unicode':
                                                                        body += str(temp['#text'].encode('ascii',
                                                                                                         'ignore'))
                                                                    else:
                                                                        body += str(temp['#text'])

                                    elif key2 == u'p':
                                        for temp in key2:
                                            if temp == u'#text':
                                                if temp.__class__.__name__ == 'unicode':
                                                    body += str((key2['p']['#text']).encode('ascii', 'ignore'))
                                                else:
                                                    body += str(key2['p']['#text'])
                                            elif temp == u'p':
                                                for item in temp:
                                                    if item == u'#text':
                                                        if item.__class__.__name__ == 'unicode':
                                                            body += str((temp['#text']).encode('ascii', 'ignore'))
                                                        else:
                                                            body += str(temp['#text'])

                        if key1 == u'p':
                            if key.__class__.__name__ == "OrderedDict":
                                for key2 in key['p']:
                                    if key2.__class__.__name__ == 'OrderedDict':
                                        if '#text' in key2:
                                            if key2['#text'].__class__.__name__ == 'unicode':
                                                body += str((key2['#text']).encode('ascii', 'ignore'))
                                            else:
                                                body += str(key2['#text'])

                                    if key2 == u'#text':
                                        if key['p']['#text'].__class__.__name__ == 'unicode':
                                            body += str((key['p']['#text']).encode('ascii', 'ignore'))
                                        else:
                                            body += str(key['p']['#text'])
                        if key1 == u'#text':
                            if key1.__class__.__name__ == 'unicode':
                                body += str((key['#text']).encode('ascii', 'ignore'))
                            else:
                                body += str(key['#text'])

    # The list to return (returnList) will contain the list of contributors, the abstract text and
    # body text, and the Document with the contributors, abstract and body, and original publication date
    # for name in contributors:
    return_list = []
    doc = Document(contributors, abstract + body, original_pub_date)
    return_list.append(contributors)
    return_list.append(abstract + body)
    return_list.append(doc)
    return return_list
