from datetime import datetime

"""
Day Object
Arguments: 	dayZero - DateTime object representing the 'first' day in the system
Functions:	getDays(date) - function that takes a DateTime object and returns the delta of dayZero and this.
"""


class Day(object):
    """
    Finds the first day in the system and sets it to dayZero. All the rest of the days in the system are
    represented as their amount of time after dayZero.
    """

    def __init__(self, dayzero):
        """
        Constructor to set the first day to dayZero
        :param dayzero: The first day in the system
        """
        self.dayzero = dayzero

    def getDays(self, date):
        """
        Finds the amount of time in between the first day and the current one
        :param date: The current date
        :return: The ID of the current Date
        """
        deltaTime = -1  # the amount of days between day zero and the current day
        if isinstance(date, datetime):  # a datetime object contains month, day, and year
            deltaTime = abs(self.dayzero - date)
        else:
            print("getDays takes a DateTime object.")  # invalid argument message
        return deltaTime.days
