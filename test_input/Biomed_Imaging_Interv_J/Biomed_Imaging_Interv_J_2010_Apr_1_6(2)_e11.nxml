<!DOCTYPE article PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD v1.0 20120330//EN" "JATS-archivearticle1.dtd">
<article xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mml="http://www.w3.org/1998/Math/MathML" article-type="case-report"><?properties open_access?><front><journal-meta><journal-id journal-id-type="nlm-ta">Biomed Imaging Interv J</journal-id><journal-id journal-id-type="publisher-id">biij</journal-id><journal-title-group><journal-title>Biomedical Imaging and Intervention Journal</journal-title></journal-title-group><issn pub-type="epub">1823-5530</issn><publisher><publisher-name>Department of Biomedical Imaging, Faculty of Medicine, University of Malaya, Malaysia</publisher-name></publisher></journal-meta><article-meta><article-id pub-id-type="pmid">21611033</article-id><article-id pub-id-type="pmc">3097762</article-id><article-id pub-id-type="doi">10.2349/biij.6.2.e11</article-id><article-categories><subj-group subj-group-type="heading"><subject>Case Report</subject></subj-group></article-categories><title-group><article-title>Left main renal artery entrapment by diaphragmatic crura: spiral CT angiography</article-title></title-group><contrib-group><contrib contrib-type="author"><name><surname>Singham</surname><given-names>S</given-names></name><degrees>MBBS, LLB</degrees></contrib><contrib contrib-type="author"><name><surname>Murugasu</surname><given-names>P</given-names></name><degrees>MBBS, FRCR, FRANZCR</degrees></contrib><contrib contrib-type="author"><name><surname>MacIntosh</surname><given-names>J</given-names></name><degrees>PhD, BMed, FRANZCR</degrees></contrib><contrib contrib-type="author"><name><surname>Murugasu</surname><given-names>P</given-names></name><degrees>MBBS, FRCR, FRANZCR</degrees></contrib><contrib contrib-type="author"><name><surname>Deshpande</surname><given-names>A</given-names></name><degrees>MBBS, FRCS</degrees></contrib></contrib-group><aff id="A1">Department of Medical Imaging, John Hunter Hospital, Newcastle, Australia</aff><author-notes><corresp id="COR1"><label>*</label> Present address: John Hunter Hospital, Lookout Rd., New Lambton, NSW Australia. E-mail: (Please contact Managing Editor).</corresp></author-notes><pub-date pub-type="epub"><day>01</day><month>4</month><year>2010</year></pub-date><pub-date pub-type="collection"><season>Apr-Jun</season><year>2010</year></pub-date><volume>6</volume><issue>2</issue><elocation-id>e11</elocation-id><history><date date-type="received"><day>19</day><month>6</month><year>2009</year></date><date date-type="rev-recd"><day>01</day><month>11</month><year>2009</year></date><date date-type="accepted"><day>16</day><month>11</month><year>2009</year></date></history><permissions><copyright-statement>&#x000a9; 2010 Biomedical Imaging and Intervention Journal</copyright-statement><copyright-year>2010</copyright-year><license license-type="open-access" xlink:href="http://creativecommons.org/licenses/by/2.5/"><license-p>This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited.</license-p></license></permissions><abstract><p>Entrapment of renal artery by the diaphragmatic crus is a rare cause of renal artery stenosis. Spiral computed tomography angiography provides a definitive diagnosis and shows the precise relationship of the artery to the diaphragmatic crus. The authors present a case of hypertension developing in a young 20-year-old female due to entrapment of the left renal artery by the diaphragmatic crus. This condition should be considered in young hypertensive patients with renal artery stenosis without cardiovascular risk factors.</p></abstract><kwd-group><title>Keywords</title><kwd>renal artery stenosis</kwd><kwd>spiral computed tomography angiography</kwd></kwd-group></article-meta></front><body><sec sec-type="" id="s1"><title>CASE REPORT</title><p>A 20-year-old female presented with shortness of breath on modest exertion. She described several recent episodes of lower retrosternal chest pain at rest. She denied any other symptoms. She had no other significant medical history but was a moderate-to-heavy smoker. There was no relevant family history.</p><p>Physical examination was normal. Full blood count and urea, creatinine and electrolyte levels were normal. Serial troponin levels were also normal. Chest x-ray was normal. Electrocardiography (ECG) showed no evidence of ischaemic change but, instead, a hypertensive response. She was noted to become hypertensive on walking up a flight of stairs. Her resting blood pressure was 130/70 mmHg and this increased to 200/100 mmHg on walking up a flight of stairs. Biochemical investigations she underwent, including tests for secondary causes of hypertension such as hyperaldosteronism and pheochromocytoma, renin aldosterone ratio as well as urinary biogenic amine, were normal. Lipid studies, thyroid function tests, liver function tests, coagulation screening, blood glucose level and HbA<sub>1c</sub> were normal.</p><p>She was initially treated with perindopril and indapamide to control her blood pressure and the treatment was effective.</p><p>A renal Doppler ultrasound suggested the presence of an accessory left renal artery. A spiral computed tomography angiography (spiral CTA) was performed with axial and sagittal slab MIPs, curved planar reformation, and 3D MIPs.</p><p>The scan was performed using a Philips Brilliance 16-slice CT scanner with 500 ml excursion 120 kV 30 mA per second. Surview image was first obtained to locate the kidneys then a helical acquisition to include a centimetre above and a centimetre below the upper and lower poles of the kidneys, respectively, with 1 mm slices and 0.5 mm spaces in between. These images were reconstructed to 2 mm by 2 mm axial and coronal images. Contrast was injected at the rate of 4 ml per second using 75 ml of Ultravist 370.</p><p>Review of the images demonstrates two left renal arteries and a single right renal artery. The right renal artery has a normal origin and normal calibre throughout its course. The left main renal artery supplies the upper pole and the mid renal parenchyma. It has a more posterior origin and marked extrinsic compression producing a haemodynamically significant stenosis as it passes aberrantly through the left crus of the diaphragm (<xref ref-type="fig" rid="F1">Figure 1a, 1b, 1c, 1d</xref>). The left lower pole renal artery is smaller in calibre with a normal course (<xref ref-type="fig" rid="F2">Figure 2</xref>). There was decreased perfusion to the left upper pole and mid renal parenchyma (<xref ref-type="fig" rid="F3">Figure 3</xref>).</p><fig id="F1" position="float"><label>Figure 1</label><caption><p> (a) Axial, (b) sagittal and (c) coronal slab MIP and (d) curved planar reformatted images. The left main renal artery has marked extrinsic compression with haemodynamically significant stenosis due to an aberrant and posterior course through the left crus of the diaphragm (arrows).</p></caption><graphic xlink:href="biij-06-e11-g01"/></fig><fig id="F2" position="float"><label>Figure 2</label><caption><p> Coronal slab MIP. The lower pole of the left kidney is supplied by an accessory small calibre renal artery (white long arrow).</p></caption><graphic xlink:href="biij-06-e11-g02"/></fig><fig id="F3" position="float"><label>Figure 3</label><caption><p> Coronal image demonstrates decreased perfusion to the upper pole and mid left kidney (white arrow). There is normal perfusion to the left lower pole and right kidney.</p></caption><graphic xlink:href="biij-06-e11-g03"/></fig><p>Subsequently, an aortogram with bilateral renal artery selection was performed during suspended inspiration and expiration (<xref ref-type="fig" rid="F4">Figure 4a and 4b</xref>). The upper pole renal artery (main) is confirmed to arise posterior and superior to the lower pole renal artery with a sharp downward bend. There was moderate narrowing at its origin with mild post stenotic dilatation, seen best on the inspiratory acquisition (<xref ref-type="fig" rid="F4">Figure 4a and 4b</xref>). Selective angiography of the upper pole artery was attempted, but the catheter could not be passed due to sharp angulation at the origin. There was no evidence of pre-existing stenosis, atherosclerosis or fibromuscular dysplasia of the left lower pole renal artery.</p><fig id="F4" position="float"><label>Figure 4</label><caption><p> Abdominal angiogram in (a) inspiration, and (b) expiration. Images demonstrate a single right normal renal artery and a normal left lower pole renal artery. The left main renal artery to the upper pole of the left kidney was moderately narrowed (white arrow) at the origin with mild post stenotic dilation.</p></caption><graphic xlink:href="biij-06-e11-g04"/></fig><p>Unfortunately, despite best attempts, this patient was lost to follow up.</p></sec><sec sec-type="" id="s2"><title>DISCUSSION</title><p>Renal artery entrapment by the diaphragmatic crus was first described by D&#x02019;Abreu [<xref ref-type="bibr" rid="R1">1</xref>] who reported two cases proven by surgery in 1962. Since this first description, less than 20 cases have been reported in the literature. Congenital abnormalities such as abnormal musculo-tendinous fibres, high ectopic renal artery origin or hypertrophic diaphragmatic crus were found to be responsible for these entrapments [<xref ref-type="bibr" rid="R2">2</xref>].</p><p>Renal artery stenosis (RAS) is a common, correctable cause of hypertension and renal impairment. In the general hypertensive population the prevalence of this condition varies between 1 and 5%. The most common causes of RAS are atherosclerosis and fibromuscular dysplasia [<xref ref-type="bibr" rid="R3">3</xref>].</p><p>Extrinsic compression of the renal arteries leading to hypertension has been associated with abdominal aortic aneurysm [<xref ref-type="bibr" rid="R4">4</xref>], tumour, hypertrophic adrenal tissue, and psoas muscle band anomaly [<xref ref-type="bibr" rid="R5">5</xref>]. However, extrinsic compression of one or both renal arteries by the diaphragmatic crura, which is known as renal entrapment syndrome, is rare [<xref ref-type="bibr" rid="R6">6</xref>-<xref ref-type="bibr" rid="R10">10</xref>]. Compression is by fibres forming part of the crus of the diaphragm or psoas muscle impinging on the renal artery by verticalisation of the root of the renal artery. This results in stenosis (usually at the ostium of the artery). The artery follows an unusual acutely angled (sigmoid) course. This anomaly is also associated with a high origin of the renal artery from the aorta and is more common on the left side. The mechanism evoked is an anomaly of migration of the kidneys [<xref ref-type="bibr" rid="R11">11</xref>].</p><p>Clinical features suggestive of RAS include abdominal bruit, severe retinopathy, unexplained hypokalaemia, and unexplained renal impairment [<xref ref-type="bibr" rid="R3">3</xref>]. Early detection of RAS is necessary for effective treatment and to prevent end-stage renal disease [<xref ref-type="bibr" rid="R12">12</xref>].</p><p>Renal artery entrapment may be suspected on angiographic views and proven by cross-sectional imaging [<xref ref-type="bibr" rid="R2">2</xref>]. Thony et al. [<xref ref-type="bibr" rid="R2">2</xref>] demonstrated two angiographic features suggesting renal artery entrapment: renal arteries descending down and close to the aorta, and a concentric ostial stenosis in a patient free of atheroma.</p><p>Although Duplex ultrasound is an accurate examination for screening RAS [<xref ref-type="bibr" rid="R13">13</xref>], it does not allow the analysis of the relationship between the renal artery and muscular structures [<xref ref-type="bibr" rid="R2">2</xref>]. This is clearly shown in angiographic reconstructions using CT [<xref ref-type="bibr" rid="R2">2</xref>].</p><p>Although surgery and stenting have been used for treatment of renal entrapment syndrome, they are associated with surgical morbidity and stent-related complications such as bending or rupture of stents. Surgical treatment needs to be considered on a case-by-case basis in relation to the anatomy and the biological and functional data. The use of an arterial stent in the situation of muscular compression leads to a risk of bending or rupture of the stent [<xref ref-type="bibr" rid="R11">11</xref>]. In addition, movement of the diaphragm induces significant displacement of kidneys during respiration which induces both bending and torsional forces on the renal arteries. This bending may lead to stent fracture and restenosis [<xref ref-type="bibr" rid="R14">14</xref>]. An alternative is to treat with balloon angioplasty and cutting balloon angioplasty, which may have lower patency rate but fewer stent-related complications in these patients [<xref ref-type="bibr" rid="R15">15</xref>].</p><p>Bilici et al. [<xref ref-type="bibr" rid="R16">16</xref>] have investigated the use of botulinum toxin injection directly into the diaphragmatic crus under CT guidance as an alternative to surgical treatment and stenting. This method still requires further evaluation.</p></sec><sec sec-type="" id="s3"><title>CONCLUSION</title><p>Compression of a renal artery by the crus of the diaphragm (renal entrapment syndrome) should be investigated in proximal renal artery stenosis in young hypertensive patients without other cardiovascular risk factors, and where fibromuscular dysplasia is unlikely. Spiral CTA is a key investigation for identification of the renal entrapment syndrome. Once the renal entrapment syndrome is confirmed, surgical management should be a consideration. New treatment methods are being evaluated including botulinum injection, which may provide an alternative to surgical management.</p></sec></body><back><ref-list><title>REFERENCES</title><ref id="R1"><label>1</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>d' Abreu</surname></name><name><surname>Strickland</surname><given-names>B</given-names></name></person-group>
<article-title>Developmental renal-artery stenosis</article-title>
<source>Lancet</source>
<year>1962</year>
<volume>12</volume>
<issue>7255</issue>
<fpage>517</fpage>
<lpage>21</lpage>
</element-citation></ref><ref id="R2"><label>2</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Thony</surname><given-names>F</given-names></name><name><surname>Baguet</surname><given-names>JP</given-names></name><name><surname>Rodiere</surname><given-names>M</given-names></name><etal/></person-group>
<article-title>Renal artery entrapment by the diaphragmatic crus</article-title>
<source>Eur Radiol</source>
<year>2005</year>
<volume>15</volume>
<issue>9</issue>
<fpage>1841</fpage>
<lpage>9</lpage>
<pub-id pub-id-type="pmid">15778837</pub-id></element-citation></ref><ref id="R3"><label>3</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Paven</surname><given-names>G</given-names></name><name><surname>Waugh</surname><given-names>R</given-names></name><name><surname>Nicholson</surname><given-names>J</given-names></name><etal/></person-group>
<article-title>Screening tests for renal artery stenosis: a case-series from an Australian tertiary referral centre</article-title>
<source>Nephrology (Carlton)</source>
<year>2006</year>
<volume>11</volume>
<issue>1</issue>
<fpage>68</fpage>
<lpage>72</lpage>
<pub-id pub-id-type="pmid">16509936</pub-id></element-citation></ref><ref id="R4"><label>4</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Lepke</surname><given-names>RA</given-names></name><name><surname>Pagani</surname><given-names>JJ</given-names></name></person-group>
<article-title>Renal artery compression by an aortic aneurysm: an unusual cause of hypertension</article-title>
<source>AJR Am J Roentgenol</source>
<year>1982</year>
<volume>139</volume>
<issue>4</issue>
<fpage>812</fpage>
<lpage>3</lpage>
<pub-id pub-id-type="pmid">6981943</pub-id></element-citation></ref><ref id="R5"><label>5</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Lampe</surname><given-names>WT</given-names><suffix>2nd</suffix></name></person-group>
<article-title>Renovascular hypertension. A review of reversible causes due to extrinsic pressure on the renal artery and report of three unusual cases</article-title>
<source>Angiology</source>
<year>1965</year>
<volume>16</volume>
<issue>11</issue>
<fpage>677</fpage>
<lpage>89</lpage>
<pub-id pub-id-type="pmid">5844140</pub-id></element-citation></ref><ref id="R6"><label>6</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Vahdat</surname><given-names>O</given-names></name><name><surname>Creemers</surname><given-names>E</given-names></name><name><surname>Limet</surname><given-names>R</given-names></name></person-group>
<article-title>[Stenosis of the right renal artery caused by the crura of the diaphragm. Report of a case]</article-title>
<source>J Mal Vasc</source>
<year>1991</year>
<volume>16</volume>
<issue>3</issue>
<fpage>304</fpage>
<lpage>7</lpage>
<pub-id pub-id-type="pmid">1940661</pub-id></element-citation></ref><ref id="R7"><label>7</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Bacourt</surname><given-names>F</given-names></name><name><surname>Depondt</surname><given-names>JL</given-names></name><name><surname>Lacombe</surname><given-names>P</given-names></name><etal/></person-group>
<article-title>[Compression of the left renal artery by the diaphragm]</article-title>
<source>J Mal Vasc</source>
<year>1992</year>
<volume>17</volume>
<issue>4</issue>
<fpage>315</fpage>
<lpage>8</lpage>
<pub-id pub-id-type="pmid">1494061</pub-id></element-citation></ref><ref id="R8"><label>8</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Clement</surname><given-names>C</given-names></name><name><surname>Ruiz</surname><given-names>R</given-names></name><name><surname>Costa-Foru</surname><given-names>B</given-names></name><etal/></person-group>
<article-title>Extrinsic compression of the renal artery by diaphragmatic crus</article-title>
<source>Ann Vasc Surg</source>
<year>1990</year>
<volume>4</volume>
<issue>3</issue>
<fpage>305</fpage>
<lpage>8</lpage>
<pub-id pub-id-type="pmid">2340253</pub-id></element-citation></ref><ref id="R9"><label>9</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Deglise</surname><given-names>S</given-names></name><name><surname>Corpataux</surname><given-names>JM</given-names></name><name><surname>Haller</surname><given-names>C</given-names></name><etal/></person-group>
<article-title>Bilateral renal artery entrapment by diaphragmatic crura: a rare cause of renovascular hypertension with a specific management</article-title>
<source>J Comput Assist Tomogr</source>
<year>2007</year>
<volume>31</volume>
<issue>3</issue>
<fpage>481</fpage>
<lpage>4</lpage>
<pub-id pub-id-type="pmid">17538300</pub-id></element-citation></ref><ref id="R10"><label>10</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Dure-Smith</surname><given-names>P</given-names></name><name><surname>Bloch</surname><given-names>RD</given-names></name><name><surname>Fymat</surname><given-names>AL</given-names></name><etal/></person-group>
<article-title>Renal artery entrapment by the diaphragmatic crus revealed by helical CT angiography</article-title>
<source>AJR Am J Roentgenol</source>
<year>1998</year>
<volume>170</volume>
<issue>5</issue>
<fpage>1291</fpage>
<lpage>2</lpage>
<pub-id pub-id-type="pmid">9574603</pub-id></element-citation></ref><ref id="R11"><label>11</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Baguet</surname><given-names>JP</given-names></name><name><surname>Thony</surname><given-names>F</given-names></name><name><surname>Sessa</surname><given-names>C</given-names></name><etal/></person-group>
<article-title>Stenting of a renal artery compressed by the diaphragm</article-title>
<source>J Hum Hypertens</source>
<year>2003</year>
<volume>17</volume>
<issue>3</issue>
<fpage>213</fpage>
<lpage>4</lpage>
<pub-id pub-id-type="pmid">12624613</pub-id></element-citation></ref><ref id="R12"><label>12</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Glockner</surname><given-names>JF</given-names></name><name><surname>Vrtiska</surname><given-names>TJ</given-names></name></person-group>
<article-title>Renal MR and CT angiography: current concepts</article-title>
<source>Abdom Imaging</source>
<year>2007</year>
<volume>32</volume>
<issue>3</issue>
<fpage>407</fpage>
<lpage>20</lpage>
<pub-id pub-id-type="pmid">16952021</pub-id></element-citation></ref><ref id="R13"><label>13</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Nchimi</surname><given-names>A</given-names></name><name><surname>Biquet</surname><given-names>JF</given-names></name><name><surname>Brisbois</surname><given-names>D</given-names></name><etal/></person-group>
<article-title>Duplex ultrasound as first-line screening test for patients suspected of renal artery stenosis: prospective evaluation in high-risk group</article-title>
<source>Eur Radiol</source>
<year>2003</year>
<volume>13</volume>
<issue>6</issue>
<fpage>1413</fpage>
<lpage>9</lpage>
<pub-id pub-id-type="pmid">12764659</pub-id></element-citation></ref><ref id="R14"><label>14</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Draney</surname><given-names>MT</given-names></name><name><surname>Zarins</surname><given-names>CK</given-names></name><name><surname>Taylor</surname><given-names>CA</given-names></name></person-group>
<article-title>Three-dimensional analysis of renal artery bending motion during respiration</article-title>
<source>J Endovasc Ther</source>
<year>2005</year>
<volume>12</volume>
<issue>3</issue>
<fpage>380</fpage>
<lpage>6</lpage>
<pub-id pub-id-type="pmid">15943515</pub-id></element-citation></ref><ref id="R15"><label>15</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Chua</surname><given-names>SK</given-names></name><name><surname>Hung</surname><given-names>HF</given-names></name></person-group>
<article-title>Renal artery stent fracture with refractory hypertension: a case report and review of the literature</article-title>
<source>Catheter Cardiovasc Interv</source>
<year>2009</year>
<volume>74</volume>
<issue>1</issue>
<fpage>37</fpage>
<lpage>42</lpage>
<pub-id pub-id-type="pmid">19530182</pub-id></element-citation></ref><ref id="R16"><label>16</label><element-citation publication-type="journal">
<person-group person-group-type="author"><name><surname>Bilici</surname><given-names>A</given-names></name><name><surname>Karcaaltincaba</surname><given-names>M</given-names></name><name><surname>Ilica</surname><given-names>AT</given-names></name><etal/></person-group>
<article-title>Treatment of hypertension from renal artery entrapment by percutaneous CT-guided botulinum toxin injection into diaphragmatic crus as alternative to surgery and stenting</article-title>
<source>AJR Am J Roentgenol</source>
<year>2007</year>
<volume>189</volume>
<issue>3</issue>
<fpage>W143</fpage>
<lpage>5</lpage>
<pub-id pub-id-type="pmid">17715081</pub-id></element-citation></ref></ref-list></back></article>