"""
The purpose of this code is to count the number of documents within the corpus
"""
from __future__ import print_function

import glob

input_path = '../../data_files/articles.A-B/*/'
count = 0
for filename in glob.glob(input_path + '*xml'):
    count += 1

print('Number of documents: ' + str(count))