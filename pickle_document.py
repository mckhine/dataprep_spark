__author__ = 'jacob'
try:
    import cPickle as pickle
except ImportError:
    import pickle
from document import Document

"""
The process of pickling and unpickling Document objects.
Pickling converts a Python object into a character stream and writes it to a file
so the object can be re-constructed in another python script.
"""


# creates test documents in a list
# documents = [Document('jacob', 'some content', '03_04_14'), Document('moldrup', 'more content', '04_05_15')]
def save_docs(documents, filename):
    """
    Save Document objects to a file
    :param documents: The documents to be saved
    :param filename: The filename where the pickled objects will be saved
    """
    # where pickled objects will be stored
    fileobject = open(filename, 'wb')
    # pickles Document objects to file called test
    pickle.dump(documents, fileobject, 2)
    fileobject.close()


def load_docs(filename):
    """
    Loads saved Document objects from previously saved file
    :param filename: The name of the file to be opened
    :return: The uploaded pickled object(s)
    """
    fileobject = open(filename, 'r')
    # loads pickled objects back
    loaded_docs = pickle.load(fileobject)
    fileobject.close()
    return loaded_docs
