__author__ = 'Jacob Moldrup'

from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

"""
Date (MM/DD/YYYY): 04/08/2015
Has a constructor for an object: Document:
properties: content, author and date (publication date).
functions:  remove_stop_words(self)
            stem(self)
            lemmatisation(self)
            remove_punctuation(self)
            clean_content(self)
"""


class Document(object):
    """A each document in the corpus is represented by this object.
    Documents have the following Properties:

    Attributes:
        author: a dictionary with the name as the key and the ID as the value.
        content: contains the titles, body, intro, abstract and other needed information without XML tags
        date: earliest publication date

    """

    def __init__(self, author, content, date):
        """
        Constructs a document object.
        :param author: dictionary with author's name as key and ID as value.
        :param content: content of a document
        :param date: earliest date the document was published (the earliest date listed in the document)
        :return:
        """
        self.author = author
        self.content = content
        self.date = date

    def remove_stop_words(self):
        """
        Performs wordstop removal on an object's content.
        :return self.content: the object's content without stop words
        """
        words = stopwords.words('english')
        self.content = [w for w in self.content if w not in words]

    def stem(self):
        """
        Performs stemming on an object's content.
        Gets rid of the suffix of the word. Includes removal of derivational affixes
        (an affix where one word is formed (derived from another).
        :return self.content: the objects content's stemmed words
        """
        list_return = []
        stems = SnowballStemmer('english')
        for word in self.content:
            list_return.append(stems.stem(word))
        self.content = list_return

    def lemmatisation(self):
        """
        Performs word lemmatisation on the object's content.
        Reduced words to a base form (like stemming).
        :return self.content: the objects content lemmatized
        """
        content_lemmatized = []
        wordnet_lemmatizer = WordNetLemmatizer()
        for word in self.content:
            content_lemmatized.append(wordnet_lemmatizer.lemmatize(word))
        self.content = content_lemmatized

    def remove_punctuation(self):
        """
        Removes punctuation from an object's content.
        :return:self.content: the objects content without punctuation
        """
        alpha_num = ''
        for char in self.content:

            # isalnum() checks for alphanumerical characters and if there is at least one character
            # and returns a boolean
            if char.isalnum() or char is ' ' or char is '\'' or char is '/':
                alpha_num += char
        self.content = alpha_num

    def clean_content(self):
        """
        Removes punctuation if the content of the object is a string and then turns it into a list.
        Performs lemmatization, stemming and stop-word removal on the object's content.
        :return: self.content: the cleaned-out object's content
        """
        # If the content is a string
        if isinstance(self.content, str):
            # Removes the punctuation from the text
            self.content.remove_punctuation()

            # Splits the text by space
            self.content = self.content.split(' ')
        self.content.lemmatisation()
        self.content.stem()
        self.content.remove_stop_words()
