"""
The main method. Will call/run the other methods.

When running the code, two command line arguments are needed: the path with the data set and
 the number of times a word needs to occur in order for it to be included in the dictionary,
For example: ../data_files/3_Biotech/ 10

If there are sub-folders within the folder that needs to be looked through, please add '*/'
to the end of the path. For example: ../data_files/3_Biotech/*/
"""

from __future__ import print_function
import glob
import time
from threading import *
from author_ID import assign_id
from xmlparser import parse_xml_docs
from nltk_wiki import clean_up_words
from multiprocessing import Pool


def run_time(input_path=None, output_path=None, common_count=None):
    contributors = []  # List of contributors
    word_list = []  # List of words in documents that occur at least the number of times given in the command line
    dates = []  # List of dates within documents
    total_list = []  # List of everything (unclean data)
    word_count_dict = {}  # Dictionary of each word and how many times it occurs
    start_time = time.time()

    # output_path = the path to the output files
    if output_path is None or not isinstance(output_path, str):
        output_path = 'output/'

    # input_path = the path to the input files
    if input_path is None or not isinstance(input_path, str):
        input_path = '../../data_files/articles.A-B/*/'
        # input_path = 'test_input/*/'

    # common_count = the minimum number of times a word needs to occur to be considered relevant
    if common_count is None or not isinstance(common_count, int):
        common_count = 10

    print('Parsing XML files...')


    """
    Here is where we begin the Multithreading process. As the most efficient run was with four processes, that is what
    we have here. The pool of processes calls the method process_files and maps the results to one list. After that, we
    must divide the individual sub-lists within temp_list into three separate lists and a dictionary.
    The pool process acts like a for loop.
    """
    pool = Pool(processes=4)
    temp_list = pool.map(process_files, glob.glob(input_path + '*xml'))
    pool.close()
    pool.join()

    for item in temp_list:
        if item:
            for list_item in item:
                id = list_item.pop(0)
                if id == 1:
                    # print (list_item)
                    for doc in list_item:
                        total_list.append(doc)
                elif id == 2:
                    for author in list_item:
                        if author not in contributors:
                            contributors.append(author)
                elif id == 3:
                    dates.append(list_item[0])
                elif id == 4:
                    for word in list_item:
                        # Only add words that are not already in the dictionary and are not digits
                        if word not in word_count_dict and not word.isdigit():
                            word_count_dict[word] = 1

                        # If the word already exists in the dictionary then we increment its count
                        elif word in word_count_dict:
                            word_count_dict[word] += 1

    print('Calculating most common words...')

    # We are only interested in words that have a count greater than the one given in the command line
    common_words = dict((k, v) for k, v in word_count_dict.items() if v >= int(common_count))

    # Put all of the common words we have found into a list
    for word in common_words:
        word_list.append(word)

    word_dict = assign_id(word_list)  # Assign IDs to the most common words, now have a dictionary with {word: ID}
    author_dict = assign_id(contributors)  # Assign IDs to the authors, now have dictionary with {name: ID}

    """
    Here is where we begin the Multithreading process. We have four Threads, one four each method we need to call.
    The arguments of each Thread are the method to call and the arguments of that method. We put the Threads into a list
    so we can easily start them and join them (or wait for them to finish)
    """
    threads = []
    thread1 = Thread(target=write_dictionary, args=(output_path, word_list, word_dict))
    thread2 = Thread(target=write_author_dict, args=(output_path, contributors, author_dict))
    thread3 = Thread(target=write_docs_author_lists, args=(output_path, total_list, author_dict, word_dict))
    thread4 = Thread(target=write_dates, args=(output_path, dates))
    threads.append(thread1)
    threads.append(thread2)
    threads.append(thread3)
    threads.append(thread4)
    for t in threads:
        t.start()

    for t in threads:
        t.join()

    elapsed_time = time.time() - start_time
    print('Elapsed time: ' + str(elapsed_time))


def process_files(filename):
    """
    This method calls parse_xml_docs() to parse the files. An ID is appended to the beginning of each sub-list so
    they can be sorted later.
    :param filename: The file we are working with
    :return: A list containing everything
    """
    total_list_local = []
    contributors_local = []
    dates_local = []  # List of datetime objects
    words_local = []  # A list of words
    temp_list = []

    with open(filename) as fd:
        unclean_list = parse_xml_docs(fd)  # Get the un-cleaned data using parse_xml_docs(string) in xml_parser.py

        # If unclean list is None, there are no contributors/authors, so we ignore the document
        if unclean_list is None:
            return

        total_list_local.append(unclean_list)  # All of the data is appended to total_list

        # Authors are only added to the list of contributors if they are not already there
        for author in unclean_list[0]:
            if author not in contributors_local:
                contributors_local.append(author)

        for word in clean_up_words(unclean_list[1]):
            if not word.isdigit():
                words_local.append(word)

        # Store the date to the list of dates
        dates_local.append(unclean_list[2].date)

        temp_list.append([1] + total_list_local)
        temp_list.append([2] + contributors_local)
        temp_list.append([3] + dates_local)
        temp_list.append([4] + words_local)

        return temp_list


def write_dictionary(output_path, word_list, word_dict):
    """
    This method writes the file dictionary.txt, which has a relevant word with its corresponding ID on each line
    :param output_path: The path of the directory to write to
    :param word_list: The list of relevant words
    :param word_dict: The dictionary of relevant words mapping to their IDs
    :return:
    """
    print("Saving common words to text...")
    with open(output_path + 'dictionary.txt', 'w') as f:
        for word in word_list:
            f.write(str(word_dict[str(word)]) + '\t' + word + '\n')


def write_author_dict(output_path, contributors, author_dict):
    """
    This method writes the file author_dict.txt, which has an author with its corresponding ID on each line
    :param output_path: The path of the directory to write to
    :param contributors: The list of authors
    :param author_dict:  This dictionary of authors mapping to their IDs
    :return:
    """
    print("Saving authors to text...")
    with open(output_path + 'author_dict.txt', 'w') as f:
        for author in contributors:
            f.write(str(author_dict[str(author).lower()]) + '\t' + author + '\n')


def write_docs_author_lists(output_path, total_list, author_dict, word_dict):
    """
    This method writes the files author_list.txt and docs.txt. In author_list.txt, each line (starting at 0)
    represents the document ID, and, on each line, there is a list of the corresponding author IDs.
    In docs.txt, each line (starting at 0) represents the document ID, and, on each line, there is a corresponding list
    of relevant word IDs.
    :param output_path: The path of the directory to write to
    :param total_list: The total list of objects
    :param author_dict: The dictionary of authors mapping to their IDs
    :param word_dict: The dictionary of relevant words mapping to their IDs
    :return:
    """
    print("Saving doc -> author and doc -> word mappings to text...")
    with open(output_path + 'author_list.txt', 'w') as author_file:
        with open(output_path + 'docs.txt', 'w') as doc_file:
            index = -1
            for uc_list in total_list:
                index += 1
                author_list_line = ''
                docs_list_line = ''
                list_of_author_ids = []

                # Getting the IDs of the authors who worked on the particular document
                for author in uc_list[0]:
                    if author.__class__.__name__ == 'unicode':
                        author = author.encode('ascii', 'ignore')
                    name = str(author_dict[str(author).lower()])
                    author_list_line += name + ' '

                    # Do not let there be duplicate authors in one file
                    if name not in list_of_author_ids:
                        list_of_author_ids.append(name)

                used_words = []  # Keep a list of words that we have already used

                # Getting the IDs of the words that appear in that document
                for word in clean_up_words(uc_list[1]):
                    word = str(word).lower()

                    # Don't include words multiple times in the same document
                    if word in word_dict:
                        if word not in used_words:
                            docs_list_line += str(word_dict[str(word)]) + ' '
                            used_words.append(word)

                author_file.write(author_list_line + '\n')
                doc_file.write(docs_list_line + '\n')


def write_dates(output_path, dates):
    """
    This method writes the file date_docs.txt, which contains a date object with its ID on every line
    :param output_path: The path of the directory to write to
    :param dates: The list of date objects
    :return:
    """
    with open(output_path + 'date_docs.txt', 'w') as date_file:
        date_format = '%Y-%m-%d'  # The format to store the date as
        doc_id_num = 0  # The documents should be in the same order as their IDs
        for date in dates:
            date_docs_line = str(date.strftime(date_format)) + '\t' + str(doc_id_num) + '\n'
            date_file.write(date_docs_line)
            doc_id_num += 1

if __name__ == "__main__":
    run_time()