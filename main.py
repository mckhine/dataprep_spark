"""
The main method. Will call/run the other methods.

When running the code, two command line arguments are needed: the path with the data set and
 the number of times a word needs to occur in order for it to be included in the dictionary,
For example: ../data_files/3_Biotech/ 10

If there are sub-folders within the folder that needs to be looked through, please add '*/'
to the end of the path. For example: ../data_files/3_Biotech/*/
"""

from __future__ import print_function

import glob
import time

from author_ID import assign_id
from xmlparser import parse_xml_docs
from nltk_wiki import clean_up_words


def run_time(input_path, output_path, common_count):
    start_time = time.time()
    contributors = []  # List of contributors
    word_list = []  # List of words in documents that occur at least the number of times given in the command line
    documents = []  # List of document objects
    dates = []  # List of dates within documents
    total_list = []  # List of everything (unclean data)
    word_count_dict = {}  # Dictionary of each word and how many times it occurs

    # output_path = the path to the output files
    if output_path is None or not isinstance(output_path, str):
        output_path = 'output/'

    # input_path = the path to the input files
    if input_path is None or not isinstance(input_path, str):
        # input_path = 'test_input/*/'
        input_path = '../../data_files/articles.A-B/*/'

    # common_count = the minimum number of times a word needs to occur to be considered relevant
    if common_count is None or not isinstance(common_count, int):
        common_count = 10
    # destination = the path to the files
    # common_count = the minimum number of times a word needs to occur in order to be stored

    print('Parsing XML files...')

    # This for loop goes through every file using glob and stores the information
    for filename in glob.glob(input_path + '*xml'):
        with open(filename) as fd:
            unclean_list = parse_xml_docs(fd)  # Get the un-cleaned data using parse_xml_docs(string) in xml_parser.py

            # If unclean list is None, there are no contributors/authors, so we ignore the document
            if unclean_list is None:
                continue

            total_list.append(unclean_list)  # All of the data is appended to total_list

            # Authors are only added to the list of contributors if they are not already there
            for author in unclean_list[0]:
                if author not in contributors:
                    contributors.append(author)

            # Clean the words and count the number of occurrences of each word.
            for word in clean_up_words(unclean_list[1]):

                # Only add words that are not already in the dictionary and are not digits
                if word not in word_count_dict and not word.isdigit():
                    word_count_dict[word] = 1

                # If the word already exists in the dictionary then we increment its count
                elif word in word_count_dict:
                    word_count_dict[word] += 1

            # Store the document objects to documents[] and all the corresponding dates to dates[]
            documents.append(unclean_list[2])
            dates.append(unclean_list[2].date)

    print('Calculating most common words...')

    # We are only interested in words that have a count greater than the one given in the command line
    common_words = dict((k, v) for k, v in word_count_dict.items() if v >= int(common_count))

    # Put all of the common words we have found into a list
    for word in common_words:
        word_list.append(word)

    word_dict = assign_id(word_list)  # Assign IDs to the most common words, now have a dictionary with {word: ID}
    author_dict = assign_id(contributors)  # Assign IDs to the authors, now have dictionary with {name: ID}

    print("Saving common words to text...")

    # Takes care of the dictionary.txt file, which has a word ID followed by a tab and the word on each line
    with open(output_path + 'dictionary.txt', 'w') as f:
        for word in word_list:
            f.write(str(word_dict[str(word)]) + '\t' + word + '\n')

    print("Saving authors to text...")

    # Takes care of the author_dict.txt file, which has an author ID followed by a tab and the author name
    with open(output_path + 'author_dict.txt', 'w') as f:
        for author in contributors:
            f.write(str(author_dict[str(author).lower()]) + '\t' + author + '\n')

    print("Saving doc -> author and doc -> word mappings to text...")

    # In author_list.txt, each line is the ID of the authors for that document
    with open(output_path + 'author_list.txt', 'w') as author_file:

        # In docs.txt, each line has the IDs of the words that occur more than the given number
        # (the command line argument) of times in that document.
        with open(output_path + 'docs.txt', 'w') as doc_file:

            index = -1
            for uc_list in total_list:
                index += 1
                author_list_line = ''
                docs_list_line = ''
                list_of_author_ids = []

                # Getting the IDs of the authors who worked on the particular document
                for author in uc_list[0]:
                    if author.__class__.__name__ == 'unicode':
                        author = author.encode('ascii', 'ignore')
                    name = str(author_dict[str(author).lower()])
                    author_list_line += name + ' '

                    # Do not let there be duplicate authors in one file
                    if name not in list_of_author_ids:
                        list_of_author_ids.append(name)

                used_words = []  # Keep a list of words that we have already used

                # Getting the IDs of the words that appear in that document
                for word in clean_up_words(uc_list[1]):
                    word = str(word).lower()

                    # Don't include words multiple times in the same document
                    if word in word_dict:
                        if word not in used_words:
                            docs_list_line += str(word_dict[str(word)]) + ' '
                            used_words.append(word)
                """
                if not used_words:
                    del dates[index]
                    index -= 1
                    total_list.remove(uc_list)
                    continue
                """

                author_file.write(author_list_line + '\n')
                doc_file.write(docs_list_line + '\n')

    # date_docs.txt has the date a document was created followed by a tab and the document ID
    with open(output_path + 'date_docs.txt', 'w') as date_file:
        date_format = '%Y-%m-%d'  # The format to store the date as
        doc_id_num = 0  # The documents should be in the same order as their IDs
        for date in dates:
            date_docs_line = str(date.strftime(date_format)) + '\t' + str(doc_id_num) + '\n'
            date_file.write(date_docs_line)
            doc_id_num += 1

    elapsed_time = time.time() - start_time
    print('Elapsed time: ' + str(elapsed_time))

if __name__ == "__main__":
    run_time(None, None, None)
