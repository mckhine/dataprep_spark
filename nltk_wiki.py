import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer


def stemWords(words):
    """
    Stem the lemmatized words
    :param words: The list of un-stemmed words
    :return: The new list of stemmed words
    """
    stemmer = SnowballStemmer("english")
    stemmedWords = []

    for word in words:
        stemmedWords.append(stemmer.stem(word))
    return stemmedWords


def lemmatizeWords(words):
    """
    Lemmatize the words
    :param words: The list of un-lemmatized words
    :return: The new list of lemmatized words
    """
    lemmatizedWords = []
    lemmatizer = WordNetLemmatizer()

    for word in words:
        lemmatizedWords.append(lemmatizer.lemmatize(word))
    return lemmatizedWords


def clean_up_words(input):
    """
    Opens and reads the wiki article, putting the text in a list before
    removing all the stopwords (such as 'and', 'or', 'not', etc)
    :param input: The text to be added to the list
    :return: The list of words from the text with the stopwords removed and
    the rest of the words stemmed and lemmatized.
    """

    # Setup tokenizer and open wiki article.
    tokenizer = RegexpTokenizer(r'\w+')

    # Read in all of the text
    words_with_stops = tokenizer.tokenize(input)

    # Remove stop words.
    stop_words = stopwords.words('english')  # Create a list of all the stopwords
    words = []

    # If a word in the text is not in the list of stopwords, append it to the
    # new list

    for word in words_with_stops:
        word_has_digit = False
        for char in word:
            if char.isdigit():
                word_has_digit = True
                pass
        if word not in stop_words and not word_has_digit:
            words.append(word)

    # Return stemming of lemmatized words
    return stemWords(lemmatizeWords(words))
